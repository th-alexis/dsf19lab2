package edu.uprm.cse.datastructures.list.test;
import static org.junit.Assert.*;

import org.junit.Test;

import edu.uprm.cse.datastructures.list.ArrayList;
import edu.uprm.cse.datastructures.list.List;
import edu.uprm.cse.datastructures.list.problems.FindMinValue;
import edu.uprm.cse.datastructures.list.problems.TotalCount;

public class ArrayListTester {

	@Test
	public void testReplaceAll1() {
		List<String> L = new ArrayList<String>();
		L.add("Kim");
		L.add("Ned");
		L.add("Ron");
		L.add("Jil");
		L.add("Amy");
		L.add("Ron");
		L.add("Ron");
		
		int c = L.replaceAll("Ron", "Apu");
		assertTrue("Should return count of 3", c == 3);
	}
	@Test
	public void testReplaceAll2() {
		List<String> L = new ArrayList<String>();
		L.add("Kim");
		L.add("Ned");
		L.add("Ron");
		L.add("Jil");
		L.add("Amy");
		L.add("Ron");
		L.add("Ron");
		List<String> L2 = new ArrayList<String>();
		L2.add("Kim");
		L2.add("Ned");
		L2.add("Apu");
		L2.add("Jil");
		L2.add("Amy");
		L2.add("Apu");
		L2.add("Apu");
		boolean same = true;
		L.replaceAll("Ron", "Apu");
		for(int i = 0; i < L.size(); i++) {
			if(!(L.get(i).equals(L2.get(i)))) {
				same = false;
				break;
			}
		}
		assertTrue("List should be altered", same);
	}
	@Test
	public void testReplaceAll3() {
		List<String> L = new ArrayList<String>();
		L.add("Kim");
		L.add("Ned");
		L.add("Ron");
		L.add("Jil");
		L.add("Amy");
		L.add("Ron");
		L.add("Ron");
		L.add("Ron");
		L.add("Ron");
		L.add("Ron");
		L.add("Ron");
		L.add("Ronny");
		L.add("Amy");
		
		int c = L.replaceAll("Ron", "He who must not be named.");
		assertTrue("Should return count of 7", c == 7);
		
	}
	@Test
	public void testReplaceAll4() {
		List<String> L = new ArrayList<String>();
		L.add("Kim");
		L.add("Ned");
		L.add("Ron");
		L.add("Jil");
		L.add("Amy");
		L.add("Ron");
		L.add("Ron");
		
		int c = L.replaceAll("Deku", "Rogers");
		assertTrue("Should return count of 0", c == 0);
	}
	@Test
	public void testReplaceAll5() {
		List<Integer> L = new ArrayList<Integer>();
		L.add(1);
		L.add(5);
		L.add(2);
		L.add(1);
		L.add(67);
		L.add(1);
		L.add(0);
		L.add(1);
		
		int c = L.replaceAll(1, 111);
		assertTrue("Should return count of 4", c == 4);
	}
	@Test
	public void testReplaceAll6() {
		List<Integer> L = new ArrayList<Integer>();
		
		int c = L.replaceAll(1, 111);
		assertTrue("Should return count of 0", c == 0);
	}
	@Test
	public void testReverse1() {
		List<String> L = new ArrayList<String>();
		L.add("Kim");
		L.add("Ned");
		L.add("Ron");
		L.add("Jil");
		L.add("Amy");
		L.add("Ron");
		
		List<String> R = L.reverse();
		boolean reversed = true;
		for(int i = 0, j = (L.size()-1); i < L.size(); i++, j--) {
			if(!(L.get(i).equals(R.get(j)))) {
				reversed = false;
			}
		}
		assertTrue("The list if strings should be reversed", reversed);
	}
	@Test
	public void testReverse2() {
		List<Integer> L = new ArrayList<Integer>();
		L.add(1);
		L.add(5);
		L.add(2);
		L.add(1);
		L.add(67);
		L.add(1);
		L.add(0);
		L.add(1);
		
		List<Integer> R = L.reverse();
		boolean reversed = true;
		for(int i = 0, j = (L.size()-1); i < L.size(); i++, j--) {
			if(!(L.get(i).equals(R.get(j)))) {
				reversed = false;
			}
		}
		assertTrue("The list of integers should be reversed", reversed);
	}
	@Test
	public void testReverse3() {
		List<Integer> L = new ArrayList<Integer>();
		
		
		List<Integer> R = L.reverse();
		assertTrue("The list of integers should be reversed", R.isEmpty());
	}
	@Test
	public void testReverse4() {
		List<Integer> L = new ArrayList<Integer>();
		L.add(1);
		
		List<Integer> R = L.reverse();
		
		assertTrue("The list of integers should be reversed", L.get(0).equals(R.get(0)));
	}
	
	@Test
	public void testMinValue1() {
		List<Integer> L = new ArrayList<Integer>();
		L.add(1);
		L.add(5);
		L.add(2);
		L.add(90);
		L.add(67);
		L.add(10);
		L.add(20);
		L.add(111);
		int c = FindMinValue.findMinValue(L);
		assertTrue("The minimum value should be 1", c == 1);
	}
	@Test
	public void testMinValue2() {
		List<Integer> L = new ArrayList<Integer>();
		int c = FindMinValue.findMinValue(L);
		assertTrue("In empty list the minimum vallue should be 0", c == 0);
	}
	@Test
	public void testMinValue3() {
		List<Integer> L = new ArrayList<Integer>();
		L.add(-1);
		L.add(-5);
		L.add(-2);
		L.add(0);
		L.add(-67);
		L.add(-100);
		L.add(-20);
		L.add(-9);
		int c = FindMinValue.findMinValue(L);
		assertTrue("The minimum value should be -100", c == -100);
	}
	@Test
	public void testTotalCount1() {
		List<String> L = new ArrayList<String>();
		List<String> L2 = new ArrayList<String>();
		List<String> L3 = new ArrayList<String>();
		L.add("Kim");
		L.add("Ned");
		L.add("Ron");
		L.add("Jil");
		
		L2.add("Amy");
		L2.add("Ron");
		L2.add("Ron");
		L2.add("Ron");
		
		L3.add("Ron");
		L3.add("Ron");
		L3.add("Ron");
		L3.add("Ronny");
		L3.add("Amy");
		
		Object[] wordsArray = new Object[3];
		wordsArray[0] = L;
		wordsArray[1] = L2;
		wordsArray[2] = L3;
		
		int c = TotalCount.totalCount("Ron", wordsArray);
		assertTrue("Should return count of 7", c == 7);
	}
	@Test
	public void testTotalCount2() {
		List<String> L = new ArrayList<String>();
		List<String> L2 = new ArrayList<String>();
		List<String> L3 = new ArrayList<String>();
		L.add("Kim");
		L.add("Ned");
		L.add("Ron");
		L.add("Jil");
		
		L2.add("Amy");
		L2.add("Ron");
		L2.add("Ron");
		L2.add("Ron");
		
		L3.add("Ron");
		L3.add("Ron");
		L3.add("Ron");
		L3.add("Ronny");
		L3.add("Amy");
		
		Object[] wordsArray = new Object[3];
		wordsArray[0] = L;
		wordsArray[1] = L2;
		wordsArray[2] = L3;
		
		int c = TotalCount.totalCount("Akko", wordsArray);
		assertTrue("Should return count of 0", c == 0);
	}
	@Test
	public void testTotalCount3() {
		List<String> L = new ArrayList<String>();
		List<String> L2 = new ArrayList<String>();
		List<String> L3 = new ArrayList<String>();
		L.add("Kim");
		L.add("Ned");
		L.add("Ron");
		L.add("Jil");
		L.add("Jil");
		
		Object[] wordsArray = new Object[3];
		wordsArray[0] = L;
		wordsArray[1] = L2;
		wordsArray[2] = L3;
		
		int c = TotalCount.totalCount("Jil", wordsArray);
		assertTrue("Should return count of 2", c == 2);
	}
	@Test
	public void testTotalCount4() {
		List<String> L = new ArrayList<String>();
		List<String> L2 = new ArrayList<String>();
		List<String> L3 = new ArrayList<String>();
		
		Object[] wordsArray = new Object[3];
		wordsArray[0] = L;
		wordsArray[1] = L2;
		wordsArray[2] = L3;
		
		int c = TotalCount.totalCount("Danny", wordsArray);
		assertTrue("Should return count of 0", c == 0);
	}
}
